<?php
  require_once("clases/01.php");
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset=utf-8>
  </head>
  <body>

<pre>
** $obj1: objecto de tipo Prueba1:

<?php
  /* Se crea una instancia de la clase Prueba1, declarada en el
   * archivo clases/01.php
   */
  $obj1 = new Prueba1();
  var_dump($obj1);
?>


** $obj2: objecto de tipo Prueba2:

<?php
  /* Se crea una instancia de la clase Prueba2, declarada en el
   * archivo clases/01.php
   *
   * NOTA: Puedes declarar tantas clases desees en un mismo
   * archivo de php.
   */

  /* El método constructor de la clase Pueba2 es __construct(), el
   * cual resibe 3 parametros que son asignados a los 3 atributos
   * de la misma clase.
   */
  $obj2 = new Prueba2('Hola', 'mundo', '!' );
  var_dump($obj2);
?>

**** Accediendo a los atributos de $obj2:

<?php
  /* Puesto que los atributos atributo1 y atributo2 tienen acceso
   * privado(private) no podemos acceder a ellos, solo a atributo3
   * ya que es publico (public).
   *
   * En caso de intentar acceder a atributos privados obtendremos
   * en el log de apache un mensaje de error como:
   *   PHP Fatal error:  Cannot access private property
   *   Prueba2::$atributo1 in /srv/www/htdocs/../01.php on line x,
   *   referer: http://frame.works.com/ejemplos/06-oo/
   *
  echo "\$atributo1 = " . $obj2->atributo1;
  echo "\n";
  echo "\$atributo2 = " . $obj2->atributo2;
  echo "\n";
   */
  echo "\$atributo3 = " . $obj2->atributo3;
  echo "\n";
?>

**** Usando métodos del objeto de tipo Prueba2:

<?php
  echo "\$atributo1 = " . $obj2->get_atributo1();
  echo "\n";
  echo "\$atributo2 = " . $obj2->get_atributo2();
  echo "\n";
  echo "\n";
  $obj2->otro_metodo();
  $obj2->set_atributo1("Adios");
  echo "\n";
  $obj2->otro_metodo();
?>

</pre>

  </body>
</html>
